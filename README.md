# Project Title

**Table of Contents**

[[_TOC_]]

**Insert project description here**

**Change the URLs of the deployed app on Render**

The project is deployed on [Render][render-url].

## What's Already Included in the Django Template?

- User Authentication System:
  - [Login][render-login]
  - [User Registration][render-register]

## Prerequisites

Ensure you have the following software already installed:
- [![python][python-badge]][python-url]
- [![NodeJs][node-badge]][node-url]

To develop the Django application, clone this repository and follow the instructions:

**Note:** SSH-based clone is recommended.

## Create Python Virtual Environment

```bash
python -m venv .venv             # for Windows git bash and Windows CMD
```

## Activate Python Virtual Environment

```bash
source .venv/bin/activate        # for UNIX and MacOS bash/zsh
```

```bash
source .venv/Scripts/activate    # for Windows git bash
```

```cmd
.venv\Scripts\activate.bat       # for Windows CMD
```

For the following steps and for development, keep the virtual environment activated all the time.

## Install Python Requirements

```bash
(.venv) $ pip install -r requirements.txt
```

## Apply Migrations

```bash
(.venv) $ python manage.py migrate
```

## Collect Static Files

```bash
(.venv) $ python manage.py collectstatic --no-input
```

## Run the Django Web Server

[![Django][django-badge]][django-url]

```bash
(.venv) $ python manage.py runserver
```

## Developer's Guide

### Install Pre-commit

**NOTE**: To run the pre-commit hooks, the python virtual environment needs to be active. Hence, it is recommended to create git commits and pushes ONLY from the shell with venv activated.

[![pre-commit][pre-commit-badge]][pre-commit-url]
[![commitizen][commitzen-badge]][commitizen-url]

The `pre-commit` hooks will be executed upon performing a `git commit` or `git push`. Each hook perform a specific check. For instance, a hook check if there is a trailing white space in the files staged. The git action (commit or push) fails if any of the pre-commit hooks fail.

```bash
(.venv) $ pre-commit install
```

The below is a sample output of a `git commit` action where the `pre-commit` is triggered and the following hooks are executed.

```bash
(.venv) $ git commit -S -m "feat(README): Added badges and moved the URLs to the end of the file"
check yaml........................................................(no files to check)Skipped
check for added large files...........................................................Passed
check for merge conflicts.............................................................Passed
fix end of files......................................................................Passed
trim trailing whitespace..............................................................Passed
don't commit to branch................................................................Passed
check toml........................................................(no files to check)Skipped
fix requirements.txt..............................................(no files to check)Skipped
Check any secret keys are accidentally added to git...................................Passed
Django Lint with ruff check.......................................(no files to check)Skipped
Django Auto Format with ruff format...............................(no files to check)Skipped
Check manage check with manage.py check...............................................Passed
Check database default with manage.py check --database default........................Passed
Check if the current branch is rebased with the default branch........................Passed
[INFO] Restored changes from /home/giri/.cache/pre-commit/patch1732622491-276231.
```
### Visual Studio Code

It's highly recommended to use [VSCode][vscode-url] for the development.

The project repository includes VSCode-specific settings ([.vscode/settings.json](.vscode/settings.json))
that are helpful for developing Django applications.

With the python virtual environment active, from the root of the repository,
open the VSCode using the following command:

```bash
(.venv) $ code .
```

Please install all the extensions (specified in [.vscode/extensions.json](.vscode/extensions.json))
recommended by VSCode upon opening the project in the code editor.

The [requirements.txt](requirements.txt) file includes the python packages required for some
of the VScode extensions. That's why VSCode needs to be opened from the bash or command
line with an active python virtual environment in which all the project-specific python packages
are installed.

Some fonts to reduce eye strain and provide better coding experience:

-   [FiraCode][fira-code-url]
-   [Jet Brains Mono][jet-brains-url]
-   [Hack][hack-url]

### GitLab CI/CD pipelines

Before performing a `git push`, run these commands to ensure that the new code changes passes
the pipeline stages:

#### Python linting

**Pipeline Stage**: _lint_

To ensure code changes meet the python coding and documentation standards, run the following
commands:

```bash
(.venv) $ ruff check . --fix
```

The above commands attempts to fix _most_ linting issues, if it raises errors that it couldn't solve, fix the lines specified in the error messages.

To pretty format the python code:
```bash
(.venv) $ ruff format .
```

#### Django migrations

**Pipeline Stage**: _build_

To ensure Django migration files are created, applied, and added to git, run the following commands:

```bash
(.venv) $ python manage.py makemigrations --check
(.venv) $ python manage.py migrate --check
```

If the any of the above commands raises an error, create migration files and add to the commit.

#### Django Tests

[![Pytest][pytest-badge]][pytest-url]

**Pipeline Stage**: _test_

To ensure code changes passes all unit tests, run the following commands:

```bash
(.venv) $ python manage.py test --parallel
```

## Team Members

**Update the last name, first name, Render app name, and URLs in the table below **

| Member ID | Role         | Last Name | First Name | Render App           |
| --------- | ------------ | --------- | ---------- | -------------------- |
| A         | Team Lead    | Last Name | First Name | [URL Text](FULL URL) |
| B         | Tech Lead    | Last Name | First Name | [URL Text](FULL URL) |
| C         | Product Lead | Last Name | First Name | [URL Text](FULL URL) |
| D         | Flex Lead    | Last Name | First Name | [URL Text](FULL URL) |

## Install Cypress

[![Cypress][cypress-badge]][cypress-url]

Open another CMD prompt, bash, or terminal, navigate to the project's root directory, and run the command:

```bash
npm install cypress
```

This will download the cypress binaries in `node_modules` directory.

It will take several minutes to download and install cypress.


## Run Cypress E2E tests

### Run the Django TestServer
In a terminal, run the Django development server:

```bash
(.venv) $ python manage.py runserver
```

To execute all the cypress tests, run the following command:
```bash
npx cypress open
```

This command will open a browser window. Select the test as `E2E`.

If there are multiple browsers on your machine, it will prompt to you to choose a browser to run Cypress tests.

Upon choosing a browser to run Cypress tests, click on the `test.cy.js` file to start the tests.


### (optional)
Below command executes all Cypress tests in the terminal (tested in Linux bash):
```bash
npx cypress run --headless
```

## (Optional) Use Docker Containers for Development

[![Docker][docker-badge]][docker-url]

See [DOCKER.md](DOCKER.md) file.

[python-badge]: https://img.shields.io/badge/python-3.12-brightgreen?style=for-the-badge&logo=python&logoColor=green
[python-url]: https://www.python.org
[node-badge]: https://img.shields.io/badge/node-22-brightgreen?style=for-the-badge&logo=npm&logoColor=green
[node-url]: https://nodejs.org/en
[cypress-badge]: https://img.shields.io/badge/cypress-latest-brightgreen?style=for-the-badge&logo=cypress&logoColor=green
[cypress-url]: https://www.cypress.io/
[django-badge]: https://img.shields.io/badge/Django-5.1-brightgreen?style=for-the-badge&logo=django&logoColor=green
[django-url]: https://www.djangoproject.com/
[pre-commit-badge]: https://img.shields.io/badge/pre--commit-enabled-brightgreen?style=for-the-badge&logo=pre-commit&logoColor=green
[pre-commit-url]: https://github.com/pre-commit/pre-commit
[commitzen-badge]: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=for-the-badge&logo=commitizen&logoColor=green
[commitizen-url]: https://github.com/commitizen-tools/commitizen
[pytest-badge]: https://img.shields.io/badge/pytest-latest-brightgreen?style=for-the-badge&logo=pytest&logoColor=green
[pytest-url]: https://docs.pytest.org/en/stable/
[docker-badge]: https://img.shields.io/badge/docker-latest-brightgreen?style=for-the-badge&logo=docker&logoColor=blue
[docker-url]: https://www.docker.com/
[vscode-url]: https://code.visualstudio.com/
[render-url]: https://django-project-template.onrender.com
[render-login]: https://django-project-template.onrender.com/accounts/login/
[render-register]: https://django-project-template.onrender.com/accounts/signup/
[fira-code-url]: https://github.com/tonsky/FiraCode
[jet-brains-url]: https://github.com/JetBrains/JetBrainsMono
[hack-url]: https://github.com/source-foundry/Hack
