python3 -m venv venv
source venv/bin/activate
pip3 install --upgrade -q pip
pip3 install -qr requirements.txt
python3 manage.py migrate >/dev/null
python3 manage.py collectstatic --no-input >/dev/null
rm -rf staticfiles/fontawesomefree/js-packages
rm -rf staticfiles/fontawesomefree/svgs
du -sh staticfiles/fontawesomefree/metadata
