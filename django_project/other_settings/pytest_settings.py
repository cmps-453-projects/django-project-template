from django_project.settings import *  # noqa

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        # Use in-memory database for faster tests
        "NAME": ":memory:",
        "TEST": {
            # Disable migrations for tests
            "MIGRATE": False,
        },
    }
}

# Other test-specific settings
PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.MD5PasswordHasher",
]
